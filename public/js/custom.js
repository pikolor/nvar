$(".slider-post").slick({
  dots: true,
  arrow: true,
  speed: 300,
  slidesToShow: 1,
  adaptiveHeight: true,
  autoplay: true,
  autoplaySpeed: 7000
});
$(".slider-main").slick({
  dots: true,
  appendArrows: ".arrow-events",
  appendDots: ".dots-events",
  nextArrow: '<i class="arrow-right ml-2 font-size--24 text-grey70"></i>',
  prevArrow: '<i class="arrow-left font-size--24 text-grey70"></i>',
  adaptiveHeight: true,
  autoplay: true,
  autoplaySpeed: 7000
});
$(".slider-32").slick({
  dots: false,
  arrow: false,
  slidesToShow: 3,
  autoplay: true,
  autoplaySpeed: 7000,
  responsive: [
    {
      breakpoint: 600,
      settings: {
        dots: true,
        slidesToShow: 2
      }
    }
  ]
});
$('.slider-events').slick({
  dots: true,
  slidesToShow: 6,
  autoplay: true,
  autoplaySpeed: 7000,
  appendArrows: ".arrow-events",
  appendDots: ".dots-events",
  nextArrow: '<i class="arrow-right ml-2 font-size--24 text-grey70"></i>',
  prevArrow: '<i class="arrow-left font-size--24 text-grey70"></i>',
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        fade: true,
      }
    },
    {
      breakpoint: 1600,
      settings: {
        slidesToShow: 4,
      }
    },
    {
      breakpoint: 4000,
      settings: {
        slidesToShow: 6,
      }
    }
  ]
});
$(".slider-4col").slick({
  slidesToShow: 4,
  arrow: false,
  dots: false,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        dots: true,
        autoplay: true,
        autoplaySpeed: 7000
      }
    }
  ]
});
$(".slider-3col").slick({
  slidesToShow: 3,
  arrow: false,
  dots: false,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        dots: true,
        autoplay: true,
        autoplaySpeed: 7000
      }
    }
  ]
});

$(".book-slide").slick({
  slidesToShow: 3,
  arrow: false,
  dots: false,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 1,
        dots: true,
        autoplay: true,
        autoplaySpeed: 7000
      }
    }
  ]
});
$(".slider-photo").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  asNavFor: ".photo-nav",
  autoplay: true,
  autoplaySpeed: 7000
});
var $status = $('.pagingInfo');
var $slickElement = $('.photo-nav');

$slickElement.on('init reInit afterChange', function (event, slick, currentSlide, nextSlide) {
  //currentSlide is undefined on init -- set it to 0 in this case (currentSlide is 0 based)
  var i = (currentSlide ? currentSlide : 0) + 1;
  $status.text(i + '/' + slick.slideCount);
});

$slickElement.slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: ".slider-photo",
  focusOnSelect: true,
  appendArrows: ".arrows",
  nextArrow: '<i class="chevron-right  font-size--24 "></i>',
  prevArrow: '<i class="chevron-left  font-size--24  "></i>',
  autoplay: true,
  autoplaySpeed: 7000,
  responsive: [
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 4,
        dots: true
      }
    }
  ]
});
$(".dropdown-menu li a").click(function () {
  var selText = $(this).text();
  $(this).parents('.btn-group').find('.dropdown-toggle').html(selText + ' <i class="chevron-down font-size--24 text-grey70">');
});
$('.dropdown-toggle').dropdown();
$('.input-number-increment').click(function () {
  var $input = $(this).parents('.input-number-group').find('.input-number');
  var val = parseInt($input.val(), 10);
  $input.val(val + 1);
});

$('.input-number-decrement').click(function () {
  var $input = $(this).parents('.input-number-group').find('.input-number');
  var val = parseInt($input.val(), 10);
  $input.val(val - 1);
})
$('.search-input').blur(function () {
    tmpval = $(this).val();
    if(tmpval == '') {
        $(".input-clear").hide();
        $(this).addClass('empty');
        $(this).removeClass('not-empty');
    } else {
        $(".input-clear").show();
        $(this).addClass('not-empty');
        $(this).removeClass('empty');
    }
});
$(".input-clear").click(function (){
  $(this).hide();
  $(".search-input").val("");
  $(".search-input").addClass('empty');
  $(".search-input").removeClass('not-empty');
});
$(".product-action a").click(function () {
  $(this).closest(".product").remove();
});
$(".share-button").click(function() {
  $(".addthis_inline_share_toolbox").toggle();
});
$(".mobile-nav-container").click(function(){
  $(".main-nav").toggle();
});
$(".toggle-item").click(function () {
  $(this).toggleClass("menu-toggle");
  $(this).siblings("a").toggleClass("selected");
  $(this).siblings("ul").toggle();
});
$(".portal-revealer").click(function () {
  console.log("asdas");
  $(this).siblings(".logo__portal-buttons__other").toggle();
});
$("#ShippingAddress").click(function() {
  $(this).parents(".col-lg").toggle();
  // $(this).parents(".form-row").toggleClass("skipShippingAddress");
  $("#inputShippingZip").val($("#inputBillingZip").val());
  $("#inputBillingZip").keyup(function() {
    $(document).find("#inputShippingZip").val($(this).val());
  });
});
$(document).ready(function () {
  $(".checkbox-lvl-1 .chevron-down").click(function() {
    $(this).toggleClass("active");
    $(this).parent(".checkbox-lvl-1").siblings(".checkbox-lvl-2").toggle();
  });
  $(".checkbox-remove").click(function() {
    $(this).parent("li").remove();
  });
  
  $(".checkbox-lvl-1 input[type=checkbox]").change(function() {
    $(this).parent(".checkbox-container").toggleClass("active");
    if ($(this).parent(".checkbox-container").hasClass("active")) {
      $(this).parents(".checkbox-lvl-1").siblings(".checkbox-lvl-2 ").find("input[type=checkbox]").prop("checked", true);
    } else {
      $(this).parents(".checkbox-lvl-1").siblings(".checkbox-lvl-2 ").find("input[type=checkbox]").prop("checked", false);
    }
  });
  $(".filter-layout i").click(function() {
    $(this).addClass("active");
    $(this).siblings().removeClass("active");
  });
  $(".filter-layout i.align-justify").click(function() {
    $("#cards").addClass("row-list");
    $("#cards").removeClass("row-grid");
  });
  $(".filter-layout i.grid").click(function() {
    $("#cards").addClass("row-grid");
    $("#cards").removeClass("row-list");
  });
  // $(".checkbox-container").click(function(){
  //   var targetCheckbox = $(this);
  //   if (!targetCheckbox.hasClass("active")) {
  //     console.log("1");
  //     targetCheckbox.children("input").prop("checked", false);
  //   } else {
  //     console.log("0");
  //     targetCheckbox.children("input").prop("checked", true);
  //     targetCheckbox.clone().appendTo($(".checkbox-current"));
  //   }
  //   targetCheckbox.toggleClass("active");
  //   console.log("click");
  // });
  
  $(function() {
    // $(document).tooltip();
  });
  $('select').niceSelect();
  $(".grid-row, *[data-value='grid-row']").click(function() {
    $("#cards").addClass("row-grid");
    $("#cards").removeClass("row-list");
  });
  $(".grid-list, *[data-value='grid-list']").click(function() {
    $("#cards").addClass("row-list");
    $("#cards").removeClass("row-grid");
  }); 
  $(".secelt-all, *[data-value='secelt-all']").click(function() {
    $("#cards").attr("category", "all");
  });

  $(".secelt-classes, *[data-value='secelt-classes']").click(function() {
    $("#cards").attr("category", "classes");
  });
  $(".select-events, *[data-value='select-events']").click(function() {
    $("#cards").attr("category", "events");
  });
  // $(".account-menu").on("click", "a", function (event) {
  //   var id = $(this).attr('href'),
  //     top = $(id).offset().top;
  //   $('body,html').animate({ scrollTop: top }, 500);
  // });
 
  $('.calendar-full').fullCalendar({
    
    header: {
      left: '',
      center: 'listMonth,listWeek,listDay',
      right: 'prev,title,next'
    },
    

    // customize the button names,
    // otherwise they'd all just say "list"
    views: {
      listDay: { buttonText: 'Day' },
      listWeek: { buttonText: 'Week' },
      listMonth: { buttonText: 'Month' }
    },

    defaultView: 'listWeek',
    defaultDate: '2018-03-12',
    navLinks: true, // can click day/week names to navigate views
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    
    events: [
      {
        title: 'All Day Event',
        start: '2018-03-01'
      },
      {
        title: 'Long Event',
        start: '2018-03-07',
        end: '2018-03-10'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2018-03-09T16:00:00'
      },
      {
        id: 999,
        title: 'Repeating Event',
        start: '2018-03-16T16:00:00'
      },
      {
        title: 'Conference',
        start: '2018-03-11',
        end: '2018-03-13'
      },
      {
        title: 'Meeting',
        start: '2018-03-12T10:30:00',
        end: '2018-03-12T12:30:00'
      },
      {
        title: 'Lunch',
        start: '2018-03-12T12:00:00'
      },
      {
        title: 'Meeting',
        start: '2018-03-12T14:30:00'
      },
      {
        title: 'Happy Hour',
        start: '2018-03-12T17:30:00'
      },
      {
        title: 'Dinner',
        start: '2018-03-12T20:00:00'
      },
      {
        title: 'Birthday Party',
        start: '2018-03-13T07:00:00'
      },
      {
        title: 'Click for Google',
        url: 'http://google.com/',
        start: '2018-03-28'
      }
    ]
  });

});
$(".collapse-custom-click-hide").click(function() {
  $(this).hide();
  $(this ).next().css({ height: "auto", overflow: "visible" });
});
$(".collapse-custom .slick-slide").click(function() {
  $(this).addClass("focus");
  $(this).siblings().removeClass("focus");
});
$("#pills-all-tab").click(function() {
  $(document).find("[data-status='shipped']").show();
  $(document).find("[data-status='paid']").show();
});
$("#pills-paid-tab").click(function() {
  $(document).find("[data-status='shipped']").hide();
  $(document).find("[data-status='paid']").show();
});
$("#pills-shipped-tab").click(function() {
  $(document).find("[data-status='shipped']").show();
  $(document).find("[data-status='paid']").hide();
});
$(".product.preview i.x").click(function() {
  $(this).closest(".product.preview").remove();
});
jQuery(function($) {
  $("[data-numeric]").payment("restrictNumeric");
  $(".cc-number").payment("formatCardNumber");
  $(".cc-exp").payment("formatCardExpiry");
  $(".cc-cvc").payment("formatCardCVC");

  $.fn.toggleInputError = function(erred) {
    this.parent(".form-group").toggleClass("has-error", erred);
    return this;
  };

  $("form").submit(function(e) {
    e.preventDefault();

    var cardType = $.payment.cardType($(".cc-number").val());
    $(".cc-number").toggleInputError(
      !$.payment.validateCardNumber($(".cc-number").val())
    );
    $(".cc-exp").toggleInputError(
      !$.payment.validateCardExpiry($(".cc-exp").payment("cardExpiryVal"))
    );
    $(".cc-cvc").toggleInputError(
      !$.payment.validateCardCVC($(".cc-cvc").val(), cardType)
    );
    $(".cc-brand").text(cardType);

    $(".validation").removeClass("text-danger text-success");
    $(".validation").addClass(
      $(".has-error").length ? "text-danger" : "text-success"
    );
  });
});